
import './App.css';
// import Greet from './components/Greet'
// import {MyGreet} from './components/Greet'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import Middle from './components/Middle/Middle'
function App() {
  return(
    <div className="App">
     
      < Header />
      < Footer />
      < Middle />
    </div>
  );
}

export default App;
