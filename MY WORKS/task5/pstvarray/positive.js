

function getPositiveNumbers() {
    var input = document.getElementById("numbersInput").value;
    var numbers = input.split(",").map(Number);
    
    const positiveNumbers = numbers.filter(function(number) {
      return number > 0;
    });
  
    document.getElementById("positiveNumbers").textContent = positiveNumbers.join(", ");
  }
  