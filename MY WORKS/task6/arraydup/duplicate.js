
function removeDuplicates() {
    var input = document.getElementById("inputArray").value;
    var array = input.split(",");
    var uniqueArray = Array.from(new Set(array));
    document.getElementById("result").innerHTML = uniqueArray.join(",");
  }